const data = {
  menuItems: [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Products',
      path: '/home',
    },
    {
      label: 'FAQ',
      path: '/faq',
    },
    {
      label: 'About Us',
      path: '/about',
    },
    {
      label: 'Policy',
      path: '/policy',
    },
    {
      label: 'Blog',
      path: '/blog',
    },
    {
      label: 'Career',
      path: '/career',
    },
    {
      label: 'Contact Us',
      path: '/contact',
    },
  ],
};
export default data;
