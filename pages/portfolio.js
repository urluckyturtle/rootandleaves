import React, { Fragment } from 'react';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import Sticky from 'react-stickynode';
import { DrawerProvider } from '../packages/common_2/src/contexts/DrawerContext';
import { portfolioTheme } from '../packages/common_2/src/theme/portfolio';
import { ResetCSS } from '../packages/common_2/src/assets/css/style';
import {
  GlobalStyle,
  ContentWrapper,
} from '../packages/common_2/src/containers/Portfolio/portfolio.style';

import BannerSection from '../packages/common_2/src/containers/Portfolio/Banner';
import Navbar from '../packages/common_2/src/containers/Portfolio/Navbar';
import AwardsSection from '../packages/common_2/src/containers/Portfolio/Awards';
import PortfolioShowcase from '../packages/common_2/src/containers/Portfolio/PortfolioShowcase';
import ProcessSection from '../packages/common_2/src/containers/Portfolio/Process';
import SkillSection from '../packages/common_2/src/containers/Portfolio/Skill';
import CallToAction from '../packages/common_2/src/containers/Portfolio/CallToAction';
import TestimonialSection from '../packages/common_2/src/containers/Portfolio/Testimonial';
import ClientsSection from '../packages/common_2/src/containers/Portfolio/Clients';
import ContactSection from '../packages/common_2/src/containers/Portfolio/Contact';
import Footer from '../packages/common_2/src/containers/Portfolio/Footer';

const Portfolio = () => {
  return (
    <ThemeProvider theme={portfolioTheme}>
      <Fragment>
        <Head>
          <title>Portfolio | A react next landing page</title>
          <meta name="Description" content="React next landing page" />
          <meta name="theme-color" content="#ec5555" />
          {/* Load google fonts */}
          <link
            href="https://fonts.googleapis.com/css?family=Raleway:300,400,400i,500,600,700,800|Roboto:300,400,400i,500,700,900"
            rel="stylesheet"
          />
        </Head>

        <ResetCSS />
        <GlobalStyle />

        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar />
            </DrawerProvider>
          </Sticky>
          <BannerSection />
          <PortfolioShowcase />
          <AwardsSection />
          <ProcessSection />
          <SkillSection />
          <CallToAction />
          <TestimonialSection />
          <ClientsSection />
          <ContactSection />
          <Footer />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default Portfolio;
